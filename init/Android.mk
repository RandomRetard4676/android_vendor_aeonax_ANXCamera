LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE	   := anxfeatures
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH  := $(TARGET_OUT_EXECUTABLES)
LOCAL_INIT_RC      := anxfeatures.rc
LOCAL_SRC_FILES    := anxfeatures.sh
include $(BUILD_PREBUILT)
