PRODUCT_SOONG_NAMESPACES += \
    vendor/aeonax/ANXCamera

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/aeonax/ANXCamera/proprietary/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,vendor/aeonax/ANXCamera/proprietary/priv-app/ANXCamera/lib/arm,$(TARGET_COPY_OUT_SYSTEM)/priv-app/ANXCamera/lib/arm) \
    $(call find-copy-subdir-files,*,vendor/aeonax/ANXCamera/proprietary/priv-app/ANXCamera/lib/arm64,$(TARGET_COPY_OUT_SYSTEM)/priv-app/ANXCamera/lib/arm64)

PRODUCT_PACKAGES += \
    ANXCamera \
    anxfeatures

PRODUCT_PROPERTY_OVERRIDES += \
    ro.miui.notch=1
